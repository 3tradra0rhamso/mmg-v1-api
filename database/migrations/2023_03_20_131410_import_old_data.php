<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        $file = __DIR__."/2023_03_20_131410_sample.sql";
        $data = file_get_contents($file);
        \DB::unprepared( $data );
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};

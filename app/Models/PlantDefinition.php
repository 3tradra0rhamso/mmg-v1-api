<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PlantDefinition extends Model
{
    use CrudTrait;
    use HasFactory;
     /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'family',
        'description'
    ];

    /**
    * Attributes that have default values.
    */
    protected $attributes = [
        'family' => 'toBeDefined',
        'description' => 'toBeDefined',
    ];

    /**
     * Get the taskDefinition for this model.
     *
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function taskDefinitions()
    {
        return $this->hasMany('App\Models\TaskDefinition','plant_definition_id','id');
    }
}
